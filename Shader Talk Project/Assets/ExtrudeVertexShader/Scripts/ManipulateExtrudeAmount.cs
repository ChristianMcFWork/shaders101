﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManipulateExtrudeAmount : MonoBehaviour {

	#region Variables

	private const string EXTRUDE_AMOUNT_NAME = "_ExtrudeAmnt";

	[Header("Speed Settings")]
	[SerializeField] private float _sinSpeed = 1f;
	[SerializeField] private float _maxSinExtrudeAmnt = 0.2f;

	private Renderer _suzanneRenderer;
	private MaterialPropertyBlock _cachedPropertyBlock;

	#endregion

	#region Init

	private void Awake() {
		_suzanneRenderer = GetComponent<Renderer>();
		_cachedPropertyBlock = new MaterialPropertyBlock();
	}

	#endregion

	#region Update

	private void Update() {
		if (_suzanneRenderer != null && _cachedPropertyBlock != null) {

			float sinValue = Mathf.Sin(Time.time * _sinSpeed);
			sinValue *= _maxSinExtrudeAmnt;
			_cachedPropertyBlock.SetFloat(EXTRUDE_AMOUNT_NAME, sinValue);

			_suzanneRenderer.SetPropertyBlock(_cachedPropertyBlock);

		}
	}

	#endregion

}
