﻿Shader ".ShaderTalk/ExtrudeVertexShader"
{
	Properties
	{
		_MainCol ("Main Colour", Color) = (1,1,1,1)
		_ExtrudeAmnt ("ExtrudeAmount",Range(-0.2,0.2)) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
		    Tags {"LightMode"="ForwardBase"}
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			#include "UnityLightingCommon.cginc"

            struct v2f
            {
                float2 uv : TEXCOORD0;
                fixed4 diff : COLOR0;
                float4 vertex : SV_POSITION;
            };
			
			fixed _ExtrudeAmnt;

			v2f vert (appdata_base v)
            {
                v2f o;
				v.vertex.xyz += v.normal.xyz * (_ExtrudeAmnt);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.texcoord;
                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.diff = nl * _LightColor0;

                o.diff.rgb += ShadeSH9(half4(worldNormal,1));
                return o;
            }		
			
			fixed4 _MainCol;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = _MainCol;
                col *= i.diff;
                return col;
            }
			ENDCG
		}
	}
}
