﻿Shader ".ShaderTalk/UnlitUVBlendShader"
{
	Properties
	{
		_MainTex ("Main Texture (RGB)",2D) = "white" {}
		_BlendAmnt ("Blend Amount",Range(0,1)) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			sampler2D _MainTex;
			fixed _BlendAmnt;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = float4(i.uv,0,1);

				fixed4 texColour = tex2D(_MainTex,i.uv);

				texColour -= 1;

				col += texColour * _BlendAmnt;

				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
