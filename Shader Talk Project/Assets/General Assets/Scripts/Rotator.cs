﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

	#region Variables

	[Header("Rotation Settings")]
	[SerializeField] private float _rotationSpeed = 10f;

	private Transform _myTF;

	#endregion

	#region Init

	private void OnEnable() {
		_myTF = transform;
	}

	#endregion

	#region Update

	private void Update() {
		if (_myTF != null) {
			_myTF.Rotate(Vector3.up, _rotationSpeed * Time.deltaTime, Space.World);
		}
	}

	#endregion

}
