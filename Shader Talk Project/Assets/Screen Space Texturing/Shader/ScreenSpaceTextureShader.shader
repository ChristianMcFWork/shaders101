﻿  Shader ".ShaderTalk/ScreenSpaceTexture" {

    Properties {
      _MainCol ("Colour", Color) = (1,1,1,1)
      _Detail ("Detail", 2D) = "gray" {}
	  _TileAmnt ("Tiling",Vector) = (1,1,1,1)
    }
    SubShader {
      Tags { "RenderType" = "Opaque" }
      CGPROGRAM
      #pragma surface surf Lambert
      struct Input {
          float2 uv_MainTex;
          float4 screenPos;
      };


      fixed4 _MainCol;
      sampler2D _Detail;
	  fixed2 _TileAmnt;


      void surf (Input IN, inout SurfaceOutput o) {
          o.Albedo = _MainCol.rgb * 3;
          float2 screenUV = IN.screenPos.xy / IN.screenPos.w;
          screenUV *= _TileAmnt.xy;
          o.Albedo *= tex2D (_Detail, screenUV).rgb;
      }
      ENDCG
    } 
    Fallback "Diffuse"
  }