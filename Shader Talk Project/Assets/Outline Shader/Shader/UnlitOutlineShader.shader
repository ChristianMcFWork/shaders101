﻿Shader ".ShaderTalk/UnlitOutlineShader"
{
    Properties
    {
		_MainCol ("Main Colour (RGB)",Color) = (1,1,1,1)
		_OutlineCol ("Outline Colour (RGB)",Color) = (1,0,0,1)
		_OutlineStrength ("Outline Strength",Range(0,0.1)) = 1
    }
    SubShader
    {

		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }

		Pass 
		{
			ZWrite Off

			CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct v2f
            {
                float4 vertex : SV_POSITION;
            };

			fixed4 _OutlineCol;
			fixed _OutlineStrength;

			v2f vert (appdata_base v)
            {
                v2f o;
				v.vertex.xyz += v.normal.xyz * _OutlineStrength;
                o.vertex = UnityObjectToClipPos(v.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = _OutlineCol;
                return col;
            }
			ENDCG
		}

        Pass
        {
            Tags {"LightMode"="ForwardBase"}

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"

            struct v2f
            {
                float2 uv : TEXCOORD0;
                fixed4 diff : COLOR0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata_base v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.texcoord;
                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.diff = nl * _LightColor0;

                o.diff.rgb += ShadeSH9(half4(worldNormal,1));
                return o;
            }
            
			fixed4 _MainCol;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = _MainCol;
                col *= i.diff;
                return col;
            }
            ENDCG
        }
    }
}
